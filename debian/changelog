glfer (0.4.2-4) unstable; urgency=medium

  * QA upload.
  * Upload to unstable.
  * debian/tests/control: dropped all tests (based in xvfb). See #988591.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Mon, 23 Aug 2021 20:31:42 -0300

glfer (0.4.2-3) experimental; urgency=medium

  * QA upload.
  * Ran wrap-and-sort.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control:
      - Added 'Rules-Requires-Root: no'.
      - Added Vcs-* fields.
      - Bumped Standards-Version to 4.5.1.
      - Removed autotools-dev from Build-depends because it is a dependency
        for debhelper.
  * debian/copyright:
      - Migrated to 1.0 format.
      - Updated all data.
  * debian/docs: added HACKING.
  * debian/glfer.desktop: added Keywords field.
  * debian/glfer.install: renamed to install.
  * debian/patches/:
      - 010_fix-print-functions.patch: added to allow the source code to build
        with DH 13.
      - 020_fix-spelling-errors.patch: added to fix spelling errors in manpage
        and final binary.
  * debian/rules: added DEB_BUILD_MAINT_OPTIONS variable to improve GCC
    hardening.
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.
  * debian/source/lintian-overrides: created because .desktop file was sent to
    upstream. Overridden as asked by lintian.
  * debian/tests/control: created to provide trivial CI tests.
  * debian/upstream/metadata: created.
  * debian/watch:
      - Bumped version to 4.
      - Improved search rule.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 26 Jun 2021 23:11:25 -0300

glfer (0.4.2-2) unstable; urgency=low

  * QA upload
  * change build-dependency on libjpeg62-dev to libjpeg-dev (closes: #641997).

 -- Ralf Treinen <treinen@debian.org>  Thu, 10 Nov 2011 13:44:54 +0100

glfer (0.4.2-1) unstable; urgency=low

  * QA upload.
  * New upstream release (closes: #568505)
  * Bump Standard-Versions to 3.9.1
  * Bump to debhelper 7
  * Rewrite the debian/rules file
  * Remove debian/dirs
  * Add ${misc:Depends} to glfer
  * Added the "Homepage" field
  * Added source format 3.0 (quilt)

 -- Lorenzo De Liso <blackz@ubuntu.com>  Wed, 15 Sep 2010 15:00:16 +0200

glfer (0.4.1-3) unstable; urgency=low

  * Retiring - set the package maintainer to Debian QA Group.

 -- Joop Stakenborg <pa3aba@debian.org>  Sun, 01 Nov 2009 19:19:59 +0000

glfer (0.4.1-2) unstable; urgency=low

  * Suggests extra-xdg-menus.
  * Remove README.Debian.

 -- Joop Stakenborg <pa3aba@debian.org>  Sun, 16 Nov 2008 19:58:06 +0100

glfer (0.4.1-1) unstable; urgency=low

  * New upstream.
  * Various lintian fixes.

 -- Joop Stakenborg <pa3aba@debian.org>  Sun, 16 Nov 2008 10:30:14 +0100

glfer (0.3.4-3) unstable; urgency=low

  * Patch by Petr Salinger to fix FTBFS on GNU/kFreeBSD, thanks!
    Closes: #379849.

 -- Joop Stakenborg <pa3aba@debian.org>  Thu, 27 Jul 2006 19:34:31 +0200

glfer (0.3.4-2) unstable; urgency=low

  * Now uses debhelper.
  * Ported to gtk+-2.0.

 -- Joop Stakenborg <pa3aba@debian.org>  Sat, 20 Aug 2005 20:06:27 +0200

glfer (0.3.4-1) unstable; urgency=low

  * New upstream release.
  * Remove mixer, so we don't conflict with csound. Closes: #274783.
  * Watch file added.

 -- Joop Stakenborg <pa3aba@debian.org>  Mon,  4 Oct 2004 19:43:56 +0200

glfer (0.3.3-1) unstable; urgency=low

  * New upstream.

 -- Joop Stakenborg <pa3aba@debian.org>  Tue, 26 Aug 2003 15:38:44 +0200

glfer (0.3.2-2) unstable; urgency=low

  * Check for sys/io.h in the configure script and only include this header
    plus associated routines (ioperm, inb, outb) if they exist. Hopefully
    this will fix compilation on non-i386. Closes: #193261.

 -- Joop Stakenborg <pa3aba@debian.org>  Fri, 11 Jul 2003 19:47:51 +0200

glfer (0.3.2-1) unstable; urgency=low

  * Initial release.

 -- Joop Stakenborg <pa3aba@debian.org>  Sun, 11 May 2003 20:17:24 +0200
